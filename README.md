# DevHun Layout

[![Compatible - MX Keys](https://img.shields.io/badge/works_with-MX_Keys-2ea44f?logo=Logitech)](https://www.logitech.com/hu-hu/products/keyboards/mx-keys-wireless-keyboard.html)
[![Compatible - MX Mini](https://img.shields.io/badge/works_with-MX_Mini-2ea44f?logo=Logitech)](https://www.logitech.com/hu-hu/products/keyboards/mx-keys-mini.html)

MacOS nemzetközi billentyűzethez magyar ékezetkiosztás.

![hunonusv1](/uploads/71d026d9bf780d5a4ebeac895174973e/hunonusv1.png)